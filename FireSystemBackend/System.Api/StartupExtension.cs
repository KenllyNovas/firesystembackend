﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System.DataModel.Contexts;
using System.Infrastructure.IRepository;
using System.Infrastructure.Repository;

namespace System.Api
{
    public static class StartupExtension
    {
        public static void ConfigureDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDataContext>(cfg =>
            {
                cfg.UseSqlServer(configuration.GetConnectionString("DbConnection"));
            });

        }
        public static void ServicesImplementations(this IServiceCollection services)
        {
            //servicios 

            #region List of Repository

            services.AddScoped<IBomberoRepository, BomberoRepository>();
            services.AddScoped<IincendioRepository, IncendioRepository>();
            services.AddScoped(typeof(IGenericRepository<>), (typeof(GenericRepository<>)));
            #endregion

        }

        public static void ConfigureAutomapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg =>
            {
                var mainAssembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(c => c.GetName().Name == "System.Infrastructure");
                cfg.AddMaps(mainAssembly);
                cfg.AllowNullCollections = true;
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
        }
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(build =>
            {
                build.AddPolicy(nameof(Startup), _ => _.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });
        }
        public static void ConfigureAddControllers(this IServiceCollection services)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddControllers().AddNewtonsoftJson(options =>
           options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
        }

        public static void ConfigureSwagger(this IServiceCollection services, IConfiguration configuration)
        {
           
            services.AddSwaggerGen();
        }
    }
}
