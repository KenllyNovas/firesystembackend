﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.DataModel.Contexts;
using System.Infrastructure.DTO;
using System.Infrastructure.Helper;
using System.Infrastructure.IRepository;

namespace System.Infrastructure.Sevices
{
    public class BomberoServices
    {
        private readonly IBomberoRepository _BomberoRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<BomberoServices> _logger;
        private int _pagination;
        public BomberoServices(IConfiguration config, IMapper mapper, IBomberoRepository bomberoRepository, ILogger<BomberoServices> logger)
        {
            _BomberoRepository = bomberoRepository;
            _mapper = mapper;
            _logger = logger;
            _pagination = Convert.ToInt32(config["Paginator"]);
        }
        public IQueryable<Bombero> GetBombero(long id)
        {
            return (IQueryable<Bombero>)_BomberoRepository.GetList(x=>x.Id==id);
        }

        public Task<List<GetBomberoDto>> Get(long id)
        {
            try
            {
                var Entities = _BomberoRepository.FindBy(x=>x.Id ==id).ToList();
                var results = _mapper.Map<List<GetBomberoDto>>(Entities);
                return Task.FromResult(results);
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al cargar  {0}, {@e}", e.Message, e);
                throw;
            }

        }

        public async Task<ResultData<GetBomberoDto>> GetByIdAsync(int id)
        {
            try
            {
                var response = await _BomberoRepository.GetByIdAsync(id);
                if (response == null)
                    return new ResultData<GetBomberoDto> { Success = false, Message = "Data no exist" };

                var model = _mapper.Map<Bombero, GetBomberoDto>(response);
                return new ResultData<GetBomberoDto> { Success = true, Data = model };
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al cargar  {0}, {@e}", e.Message, e);
                throw;
            }

        }

        public async Task<Response> Add(AddBomberoDto model)
        {
            try
            {
                var entity = await _BomberoRepository.GetByIdAsync(model.Id);
                if (entity != null)
                    return new Response { IsSuccess = false, Message = "Esta data ya existe" };

                var response = _mapper.Map<Bombero>(model);

                await _BomberoRepository.CreateAsync(response);

                var result = _mapper.Map<AddBomberoDto>(response);

                return new Response { IsSuccess = true, Result = result };
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al guardad {0}, {@e}", e.Message, e);
                return new Response { IsSuccess = false };
            }
        }

        public async Task<Response> Update(AddBomberoDto model)
        {
            try { 
            
                var entity = await _BomberoRepository.GetByIdAsync(model.Id);
                if (entity == null)
                    return new Response { IsSuccess = false, Message = "data no encontrada" };


                var response = _mapper.Map<Bombero>(model);
                await _BomberoRepository.UpdateAsync(response);

                return new Response { IsSuccess = true, Message = "Operacion realizada exitosamente." };
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al guardad {0}, {@e}", e.Message, e);
                return new Response { IsSuccess = false };
            }
        }

        public async Task<Response> Delete(int id)
        {
            try
            {
                var entity = await _BomberoRepository.GetByIdAsync(id);
                if (entity == null)
                    return new Response { IsSuccess = false, Message = "data no encontrada" };

                await _BomberoRepository.DeleteAsync(entity);
                return new Response { IsSuccess = true, Message = "Operacion realizada exitosamente." };
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al guardad {0}, {@e}", e.Message, e);
                return new Response { IsSuccess = false };
            }
        }
    }
}

