USE [dbBomberos]
GO
/****** Object:  Table [dbo].[Bomberos]    Script Date: 1/9/2023 12:05:52 p.�m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bomberos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](max) NOT NULL,
	[Apellido] [nvarchar](max) NOT NULL,
	[FechaNacimiento] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Incendios]    Script Date: 1/9/2023 12:05:52 p.�m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Incendios](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Direccion] [nvarchar](max) NOT NULL,
	[BomberoId] [int] NOT NULL,
	[Fecha] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Bomberos] ON 
GO
INSERT [dbo].[Bomberos] ([ID], [Nombre], [Apellido], [FechaNacimiento]) VALUES (1, N'Juan Pablo', N'P�rez Hernandez', CAST(N'1990-01-15' AS Date))
GO
INSERT [dbo].[Bomberos] ([ID], [Nombre], [Apellido], [FechaNacimiento]) VALUES (2, N'Mar�a extrella', N'Garc�a Marquez', CAST(N'1988-05-22' AS Date))
GO
INSERT [dbo].[Bomberos] ([ID], [Nombre], [Apellido], [FechaNacimiento]) VALUES (3, N'Luis miguel', N'Rodr�guez Encarnacion', CAST(N'1995-11-03' AS Date))
GO
INSERT [dbo].[Bomberos] ([ID], [Nombre], [Apellido], [FechaNacimiento]) VALUES (4, N'Andy Luca', N'Vargas Martinez', CAST(N'2023-09-01' AS Date))
GO
INSERT [dbo].[Bomberos] ([ID], [Nombre], [Apellido], [FechaNacimiento]) VALUES (5, N'Adriano Jose', N'Quiroz Rodriguez', CAST(N'1991-08-02' AS Date))
GO
INSERT [dbo].[Bomberos] ([ID], [Nombre], [Apellido], [FechaNacimiento]) VALUES (6, N'Kenlly Starlin', N'Novas Contreras', CAST(N'1992-05-01' AS Date))
GO
INSERT [dbo].[Bomberos] ([ID], [Nombre], [Apellido], [FechaNacimiento]) VALUES (8, N'Jose Luis', N'Perez Martinez', CAST(N'1992-08-01' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Bomberos] OFF
GO
SET IDENTITY_INSERT [dbo].[Incendios] ON 
GO
INSERT [dbo].[Incendios] ([ID], [Direccion], [BomberoId], [Fecha]) VALUES (2, N'Avenida X, Ciudad B', 2, CAST(N'2023-08-05' AS Date))
GO
INSERT [dbo].[Incendios] ([ID], [Direccion], [BomberoId], [Fecha]) VALUES (3, N'Plaza Z, Ciudad A', 3, CAST(N'2023-08-10' AS Date))
GO
INSERT [dbo].[Incendios] ([ID], [Direccion], [BomberoId], [Fecha]) VALUES (4, N'San cristobals', 4, CAST(N'2023-09-01' AS Date))
GO
INSERT [dbo].[Incendios] ([ID], [Direccion], [BomberoId], [Fecha]) VALUES (5, N'Santo Domingos', 5, CAST(N'2022-05-01' AS Date))
GO
INSERT [dbo].[Incendios] ([ID], [Direccion], [BomberoId], [Fecha]) VALUES (6, N'Los alcarrizos', 1, CAST(N'2021-01-01' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Incendios] OFF
GO
ALTER TABLE [dbo].[Incendios]  WITH CHECK ADD FOREIGN KEY([BomberoId])
REFERENCES [dbo].[Bomberos] ([ID])
GO
