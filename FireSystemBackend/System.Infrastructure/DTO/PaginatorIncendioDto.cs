﻿using System;
using System.Collections.Generic;
using System.Infrastructure.Base;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Infrastructure.DTO
{
    public class PaginatorIncendioDto : PaginatorBase
    {
        public int? CodigoBombero { get; set; }
        public string? Nombre { get; set; }
        public string? Apellido { get; set; }
        public DateTimeOffset? Fecha { get; set; }
        public DateTimeOffset? FechaNacimiento { get; set; }
    }
}
