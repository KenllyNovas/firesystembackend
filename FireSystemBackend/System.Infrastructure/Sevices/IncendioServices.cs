﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.DataModel.Contexts;
using System.Infrastructure.DTO;
using System.Infrastructure.Helper;
using System.Infrastructure.IRepository;

namespace System.Infrastructure.Sevices
{
    public class IncendioServices
    {
        private readonly IincendioRepository _iincendioRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<IncendioServices> _logger;
        private int _pagination;
        public IncendioServices(IConfiguration config, IMapper mapper, IincendioRepository iincendioRepository, ILogger<IncendioServices> logger)
        {
            _iincendioRepository = iincendioRepository;
            _mapper = mapper;
            _logger = logger;
            _pagination = Convert.ToInt32(config["Paginator"]);
        }
        public IQueryable<Incendio> GetIncendio(long id)
        {
            return _iincendioRepository.GetIncendios(id);
        }

        public Task<List<GetIncendioDto>> Get(long id)
        {
            try
            {
                var Entities = _iincendioRepository.GetIncendios(id).ToList();
                var results = _mapper.Map<List<GetIncendioDto>>(Entities);
                return Task.FromResult(results);
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al cargar  {0}, {@e}", e.Message, e);
                throw;
            }

        }
        public async Task<ResultData<GetIncendioDto>> GetByIdAsync(int id)
        {
            try
            {
                var Country = await _iincendioRepository.GetByIdAsync(id);
                if (Country == null)
                    return new ResultData<GetIncendioDto> { Success = false, Message = "Date no exist" };

                var model = _mapper.Map<Incendio, GetIncendioDto>(Country);
                return new ResultData<GetIncendioDto> { Success = true, Data = model };
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al cargar  {0}, {@e}", e.Message, e);
                throw;
            }

        }

        public async Task<Response> Add(AddIncendioDto model)
        {
            try
            {
                var entity = await _iincendioRepository.GetByIdAsync(model.Id);
                if (entity != null)
                    return new Response { IsSuccess = false, Message = "Esta incendio ya existe" };

                var response = _mapper.Map<Incendio>(model);

                await _iincendioRepository.CreateAsync(response);
                var result = _mapper.Map<AddIncendioDto>(response);

                return new Response { IsSuccess = true, Result = result };
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al guardad {0}, {@e}", e.Message, e);
                return new Response { IsSuccess = false };
            }
        }

        public async Task<Response> Update(AddIncendioDto model)
        {
            try
            {
                var entity = await _iincendioRepository.GetByIdAsync(model.Id);
                if (entity == null)
                    return new Response { IsSuccess = false, Message = "incendio no encontrada" };

                entity.Direccion = model.Direccion;
                await _iincendioRepository.UpdateAsync(entity);

                return new Response { IsSuccess = true, Message = "Operacion realizada exitosamente." };
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al guardad {0}, {@e}", e.Message, e);
                return new Response { IsSuccess = false };
            }
        }

        public async Task<Response> Delete(int id)
        {
            try
            {
                var entity = await _iincendioRepository.GetByIdAsync(id);
                if (entity == null)
                    return new Response { IsSuccess = false, Message = "incendio no encontrada" };

                await _iincendioRepository.DeleteAsync(entity);

                return new Response { IsSuccess = true, Message = "Operacion realizada exitosamente." };
            }
            catch (Exception e)
            {
                _logger.LogError("Hubo  un error al guardad {0}, {@e}", e.Message, e);
                return new Response { IsSuccess = false };
            }
        }
    }
}
