﻿using System;
using System.Collections.Generic;
using System.Infrastructure.Base;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Infrastructure.DTO
{
    public class PaginatorBomberoDto : PaginatorBase
    {
        public string? Nombre { get; set; }
        public string? Apellido { get; set; }

        public DateTimeOffset? FechaNacimiento { get; set; }
    }
}
