﻿using Microsoft.AspNetCore.Mvc;
using System.Infrastructure.DTO;
using System.Api.Errors;
using System.Infrastructure.IRepository;
using AutoMapper;
using System.DataModel.Contexts;
using System.Infrastructure.Base;
using System.Net;
using Microsoft.EntityFrameworkCore;

namespace System.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BomberoController : ControllerBase
    {
        private readonly IBomberoRepository _bomberoServices;
        protected readonly IMapper _mapper;
        public BomberoController(IBomberoRepository bomberoServices, IMapper mapper)
        {
            _bomberoServices = bomberoServices;
            _mapper = mapper;
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var list = await _bomberoServices.GetAll().Include(e => e.Incendios).ToListAsync();
            var model = _mapper.Map<List<GetBomberoDto>>(list);
            return Ok(model);
        }

        // GET: api/Bombero
        /// <summary>
        /// Obtiene un resultado paginado de los objetos de la BD.
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Take"></param>
        /// <returns></returns>
        [HttpGet("GetAllPaginate")]
        public async Task<IActionResult> GetAllPaginate([FromQuery]  PaginatorBomberoDto request)
        {
            try
            {
                var list = await _bomberoServices.GetPagedAsync(request.Page > 0 ? request.Page : 1, request.Take > 0 ? request.Take : 10,
                 x => x.OrderByDescending(o => o.Id),
                x => (request.Codigo > 0 ? x.Id == request.Codigo : true)
                && (request.Nombre != null ? x.Nombre.Contains(request.Nombre) : true)
                && (request.Apellido != null ? x.Apellido.Contains(request.Apellido) : true)
                && (request.FechaNacimiento != null ? x.FechaNacimiento == request.FechaNacimiento : true));


              if (list == null)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.NotFound, Success = false, Message = $"Datos no encontrados" });

                var response = _mapper.Map<DataCollectionDto<GetBomberoDto>>(list);

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError,Success=false ,Message = $"Hubo  un error al cargar ,{e.Message}" });
            }
        }


        // GET: api/Bombero/GetById/5
        /// <summary>
        /// Obtiene  los  Bombero atravéz de su Id.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id">Id (GUID) del objeto.</param>
        /// <response code="401">Unauthorized. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response>      
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.BadRequest, Message = "Campo Id es requerido" });

              var response = await _bomberoServices.GetAll()
             .Include(e => e.Incendios)
             .FirstOrDefaultAsync(dt => dt.Id == id);

           
                if (response == null)
                    return NotFound(new OperationResult() { StatusCode = HttpStatusCode.NotFound, Success = false, Message = $"Datos no encontrados" });

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
            }

        }

        // Post: api/Bombero/Post
        /// <summary>
        /// Metodo utilizado para guardar los Bomberos
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="401">Unauthorized. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="201">OK. Devuelve el objeto creado.</response>        
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response> 
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddBomberoDto dto)
        {   
            try
            {
                if (dto == null)
                        return BadRequest(new OperationResult() { Success = false, Message = "Datos del modelo son requeridos.", StatusCode = HttpStatusCode.BadRequest });

                var model = _mapper.Map<Bombero>(dto);

                var response = await _bomberoServices.Addasync(model);

                var resultSave = await _bomberoServices.SaveAsync();

                if (!resultSave.Success)
                    return NotFound(new OperationResult() { Success = false, Message = "Registro no se pudo guardar.", StatusCode = HttpStatusCode.BadRequest });
                
                var result = new OperationResult<Bombero>()
                {
                    Result = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Registro guardado  con éxito."
                };
                return Ok(result);

            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
            }
        }

        // Update: api/Bombero/Put
        /// <summary>
        /// Metodo utilizado para actualizar los Bomberos
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="401">Unauthorized. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="201">OK. Devuelve el objeto creado.</response>        
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response> 
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] AddBomberoDto dto)
        {
            try
            {
                if (dto == null || dto.Id <= 0)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.BadRequest ,Message="Campo Id es requerido"});

                //var model = await _bomberoServices.GetByIdAsync(id);
                var model =  _bomberoServices.FindBy(e=>e.Id ==dto.Id ).FirstOrDefault();

                if (model == null)
                    return NotFound(new OperationResult() { StatusCode = HttpStatusCode.NotFound, Message = "Datos del registro no existe" });

                 model = _mapper.Map<Bombero>(dto);

                var response = await _bomberoServices.UpdateAsync(model);
               
                var resultSave = await _bomberoServices.SaveAsync();

                if (!resultSave.Success)
                    return BadRequest(new OperationResult() { Success = false, Message = "Registro no se pudo guardar.", StatusCode = HttpStatusCode.BadRequest });



                var result = new OperationResult<Bombero>()
                {
                    Result = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Registro actualizado  con éxito."
                };
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
             
            }

        }

        // Update: api/Bombero/5
        /// <summary>
        /// Metodo utilizado para eliminar los Bomberos
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <response code="401">Unauthorized. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="201">OK. Devuelve el objeto creado.</response>        
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response> 
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if (id <=0)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.BadRequest ,Message="Campo Id es requerido"});

                var model = _bomberoServices.FindBy(e => e.Id == id).FirstOrDefault();

                var response =await _bomberoServices.DeleteAsync(model);

                var resultSave = await _bomberoServices.SaveAsync();

                if (!resultSave.Success)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = "Registro no se pudo eliminar" });


                var result =new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = true, Message = "Registro eliminado con éxito" };
               
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
            }

        }

    }
}
