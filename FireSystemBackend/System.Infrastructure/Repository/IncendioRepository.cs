﻿using System.Data;
using System.DataModel.Contexts;
using System.Infrastructure.IRepository;

namespace System.Infrastructure.Repository
{
    public class IncendioRepository : GenericRepository<Incendio>, IincendioRepository
    {
        private readonly AppDataContext _context;
        public IncendioRepository(AppDataContext context) : base(context)
        {
            _context = context;
        }
        public IQueryable<Incendio> GetIncendios(long id)
        {
            return _context.Incendios.Where(x => x.Id == id).OrderByDescending(x => x.Id);
        }

    }
}
