﻿using System.DataModel.Contexts;

namespace System.Infrastructure.IRepository
{
    public interface IBomberoRepository : IGenericRepository<Bombero>
    {
        //IQueryable<Bombero> GetBombero(long id);
        Bombero FindById(int id);
    }
}
