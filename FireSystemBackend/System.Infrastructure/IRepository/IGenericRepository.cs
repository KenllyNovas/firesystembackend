﻿using Microsoft.EntityFrameworkCore.Query;
using System.Infrastructure.Base;
using System.Linq.Expressions;

namespace System.Infrastructure.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        void Delete(int id);
        void Delete(T entity);
        void DeleteRange(IEnumerable<T> entity);
        bool SaveAll();

        Task<List<T>> UpdateRange(List<T> entity);

        Task<T> CreateRangeAsync(IEnumerable<T> entity);

        IQueryable<T> GetQueryable();

        Task<List<T>> GetAllAsync();


        Task<T> GetByIdAsync(int id);

        Task<T> CreateAsync(T entity);

        Task<OperationResult> UpdateAsync(T entity);

 
        Task<bool> ExistAsync(Guid id);
        IEnumerable<T> Pagination(int page = 1, int count = 5);
        int Count();
        Task<DataCollection<T>> GetPagedAsync(int page, int take, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);
        Task<IEnumerable<T>> GetList(Expression<Func<T, bool>> predicate);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        bool Commit();
        Task<bool> CommitAsync();
        Task<OperationResult> Addasync(T entity);
        Task<OperationResult> DeleteAsync(T entity);
        IQueryable<T> GetAll(params Expression<Func<T, object>>[] includeProperties);
        Task<OperationResult> SaveAsync();
    }
}
