﻿
using System.DataModel.Contexts;
using System.Infrastructure.IRepository;

namespace System.Infrastructure.Repository
{
    public class BomberoRepository : GenericRepository<Bombero>, IBomberoRepository
    {
        private readonly AppDataContext _context;
        public BomberoRepository(AppDataContext context) : base(context)
        {
            _context = context;
        }
        public IQueryable<Bombero> GetBombero(long id)
        {
            return _context.Bomberos.Where(x => x.Id == id).OrderByDescending(x => x.Id);
        }
        public Bombero FindById(int id)
        {
            var bombero=new Bombero();

           bombero = _context.Bomberos.FirstOrDefault(x => x.Id == id);

            return bombero;
        }
    }
}
