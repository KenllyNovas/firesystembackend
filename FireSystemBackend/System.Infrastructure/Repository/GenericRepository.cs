﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Query;
using System.Data;
using System.DataModel.Contexts;
using System.Infrastructure.Base;
using System.Infrastructure.IRepository;
using System.Linq.Expressions;
using System.Net;

namespace System.Infrastructure.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly AppDataContext _context;
        private readonly DbSet<T> _dbSet;
        public GenericRepository(AppDataContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }
        protected IQueryable<T> PrepareQuery(
           IQueryable<T> query,
           Expression<Func<T, bool>> predicate = null,
           Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           int? take = null
       )
        {
            if (include != null)
                query = include(query);

            if (predicate != null)
                query = query.Where(predicate);

            if (orderBy != null)
                query = orderBy(query);

            if (take.HasValue)
                query = query.Take(Convert.ToInt32(take));

            return query;
        }
        public virtual async Task<DataCollection<T>> GetPagedAsync(
           int page,
           int take,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy,
           Expression<Func<T, bool>> predicate = null,
           Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null
       )
        {
            var query = _context.Set<T>().AsQueryable();
            var originalPages = page;

            page--;

            if (page > 0)
                page = page * take;

            query = PrepareQuery(query, predicate, include, orderBy);

            var result = new DataCollection<T>
            {
                Items = await query.Skip(page).Take(take).ToListAsync(),
                Total = await query.CountAsync(),
                Page = originalPages
            };

            if (result.Total > 0)
            {
                result.Pages = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(result.Total) / take));
            }

            return result;
        }
        public async Task<List<T>> GetAllAsync()
        {
            return await this._context.Set<T>().ToListAsync();
        }

        public  IQueryable<T> GetAll(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbSet.AsQueryable();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public async Task<IEnumerable<T>> GetList(Expression<Func<T, bool>> predicate)
         => await _dbSet.Where(predicate).ToListAsync();
        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicate)
    => await _dbSet.AnyAsync(predicate);

        public bool Any(Expression<Func<T, bool>> predicate)
            => _dbSet.Any(predicate);
        public async Task<T> GetByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }


        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbSet.Where(predicate);
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query.AsNoTracking();
        }
        public async Task<T> CreateAsync(T entity)
        {
            await this._context.Set<T>().AddAsync(entity);
            await SaveAllAsync();
            return entity;
        }
        public virtual async Task<OperationResult> Addasync(T entity)
        {
                var entry = await _dbSet.AddAsync(entity);
           
            return new OperationResult() { Success = true, StatusCode = HttpStatusCode.Created };
           
        }

        public IQueryable<T> GetQueryable() => _dbSet.AsQueryable();
        public async Task<T> CreateRangeAsync(IEnumerable<T> entity)
        {
            await this._context.Set<T>().AddRangeAsync(entity);
            await SaveAllAsync();
            return (T)entity;
        }

        public async Task<OperationResult> UpdateAsync(T entity)
        {
           _context.Entry(entity).State = EntityState.Modified;
 
            return new OperationResult() { Success = true, StatusCode = HttpStatusCode.Created };
        }

        public async Task<List<T>> UpdateRange(List<T> entity)
        {
            this._context.Set<T>().UpdateRange(entity);

            return entity;
        }
        public async Task<OperationResult> DeleteAsync(T entity)
        {
            this._context.Set<T>().Remove(entity);

            var resultSave = await SaveAllAsync();

            if (!resultSave)
                return new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false ,Message="Registro no se pudo eliminar"};

         
                return new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = true, Message = "Registro eliminado con éxito" };
        }
        public async void DeleteRange(IEnumerable<T> entity)
        {
            this._context.Set<T>().RemoveRange(entity);
            await SaveAllAsync();
        }
        public void Delete(int id)
        {
            var entity = this._context.Set<T>().Find(id);
            if (entity != null)
            {
                Delete(entity);
            }
        }

   
        public void Delete(T entity)
        {
            this._context.Set<T>().Remove(entity);
        }
        public async Task<bool> SaveAllAsync()
        {
            return await this._context.SaveChangesAsync() > 0;
        }
        public bool SaveAll()
        {
            return this._context.SaveChanges() > 0;
        }

       

        public Task<bool> ExistAsync(Guid id)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<T> Pagination(int page = 1, int count = 5)
        {
            return _context.Set<T>().Skip((page - 1) * count).Take(count);
        }
        public int Count()
        {
            return _context.Set<T>().Count();
        }
        public IEnumerable<T> FindAll(Expression<Func<T, bool>> token)
        {
            return _context.Set<T>().Where(token);
        }
        public bool Commit()
        {
            try
            {
                return _context.SaveChanges() > 0;
            }
            catch (Exception e)
            {
                Console.Write(e);
                throw;
            }
        }


        public async Task<bool> CommitAsync()
        {

            try
            {
                var savedRegistries = await _context.SaveChangesAsync();
                bool succeeded = savedRegistries > 0;
                return succeeded;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }


        public virtual async Task<OperationResult> SaveAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
                return new OperationResult() { Success = true, StatusCode = HttpStatusCode.Created };
            }
            catch (DbUpdateException ex)
            {
                return new OperationResult() { Message = ex.Message, StatusCode = HttpStatusCode.InternalServerError };
            
            }
        }


    }
}
