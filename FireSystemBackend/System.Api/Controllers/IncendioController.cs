﻿using Microsoft.AspNetCore.Mvc;
using System.Infrastructure.DTO;
using System.Api.Errors;
using System.Infrastructure.IRepository;
using AutoMapper;
using System.DataModel.Contexts;
using System.Infrastructure.Base;
using System.Net;
using Microsoft.EntityFrameworkCore;

namespace System.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IncendioController  : ControllerBase
    {
        private readonly IincendioRepository _incendioServices;
        protected readonly IMapper _mapper;
        public IncendioController(IincendioRepository incendioServices, IMapper mapper)
        {
            _incendioServices = incendioServices;
            _mapper = mapper;
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var list = await _incendioServices.GetAll().Include(e => e.Bombero).ToListAsync();
            var model = _mapper.Map<List<GetIncendioDto>>(list);
            return Ok(model);
        }

        // GET: api/Incendio
        /// <summary>
        /// Obtiene un resultado paginado de los objetos de la BD.
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Take"></param>
        /// <returns></returns>
        [HttpGet("GetAllPaginate")]
        public async Task<IActionResult> GetAllPaginate([FromQuery] PaginatorIncendioDto request)
        {
            try
            {
               
                var list = await _incendioServices.GetPagedAsync(request.Page > 0 ? request.Page : 1, request.Take > 0 ? request.Take : 10,
                x => x.OrderByDescending(o => o.Id),
                x => (request.Codigo > 0 ? x.Id == request.Codigo : true)
                && (request.CodigoBombero != null ? x.BomberoId==request.CodigoBombero : true)
                && (request.Nombre != null ? x.Bombero.Nombre.Contains(request.Nombre) : true)
                && (request.Apellido != null ? x.Bombero.Apellido.Contains(request.Apellido) : true)
                && (request.FechaNacimiento != null ? x.Bombero.FechaNacimiento == request.FechaNacimiento : true)
                && (request.Fecha != null ? x.Fecha == request.Fecha : true),
                x => x.Include(e => e.Bombero));

                    if (list == null)
                        return NotFound(new OperationResult() { StatusCode = HttpStatusCode.NotFound, Success = false, Message = $"Datos no encontrados" });

                var response = _mapper.Map<DataCollectionDto<GetIncendioDto>>(list);

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
            }
        }


        // GET: api/Incendio/GetById/5
        /// <summary>
        /// Obtiene  los  Bombero atravéz de su Id.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id">Id (GUID) del objeto.</param>
        /// <response code="401">Unauthorized. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response>      
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.BadRequest, Message = "Campo Id es requerido" });

                var response = await _incendioServices.GetAll()
               .Include(e => e.Bombero)
               .FirstOrDefaultAsync(dt => dt.Id == id);

                if (response == null)
                    return NotFound(new OperationResult() { StatusCode = HttpStatusCode.NotFound, Success = false, Message = $"Datos no encontrados"});


                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
            }

        }
        [HttpGet("GetIncendioByBombero/{bomberoId}")]
        public async Task<ActionResult<List<GetIncendioDto>>> GetIncendioByBombero(int bomberoId)
        {
            try
            {
                var list = await _incendioServices.GetAll()
               .Where(dt => dt.BomberoId == bomberoId)
               .Include(e => e.Bombero)
               .ToListAsync();

                if (list == null)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.NotFound, Success = false, Message = $"Datos no encontrados"});

                var model = _mapper.Map<List<GetIncendioDto>>(list);

                return Ok(model);
            }
            catch (Exception ex)
            {
                return StatusCode(((int)HttpStatusCode.InternalServerError), new { Message = "Error inesperado", exception = ex });
            }
          }
            // Post: api/Incendio/Post
            /// <summary>
            /// Metodo utilizado para guardar los Bomberos
            /// </summary>
            /// <remarks>
            /// </remarks>
            /// <param name="model"></param>
            /// <response code="401">Unauthorized. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
            /// <response code="201">OK. Devuelve el objeto creado.</response>        
            /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response> 
            [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddIncendioDto dto)
        {
            try
            {
                if (dto == null)
                    return BadRequest(new OperationResult() { Success = false, Message = "Datos del modelo son requeridos.", StatusCode = HttpStatusCode.BadRequest });

                var model = _mapper.Map<Incendio>(dto);

                var response = await _incendioServices.Addasync(model);

                var resultSave = await _incendioServices.SaveAsync();

                if (!resultSave.Success)
                    return BadRequest(new OperationResult() { Success = false, Message = "Registro no se pudo guardar.", StatusCode = HttpStatusCode.BadRequest });


                var result = new OperationResult<Incendio>()
                {
                    Result = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Registro guardado  con éxito."
                };
                return Ok(result);

            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
            }
        }

        // Update: api/Incendio/Put
        /// <summary>
        /// Metodo utilizado para actualizar los Bomberos
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="401">Unauthorized. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="201">OK. Devuelve el objeto creado.</response>        
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response> 
        [HttpPut]
            public async Task<IActionResult> Put([FromBody] AddIncendioDto dto)
        {
            try
            {
                if (dto == null || dto.Id <= 0)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.BadRequest, Message = "Campo Id es requerido" });

                //var model = await _incendioServices.GetByIdAsync(id);
                var model = _incendioServices.FindBy(e => e.Id == dto.Id).FirstOrDefault();

                if (model == null)
                    return NotFound(new OperationResult() { StatusCode = HttpStatusCode.NotFound, Message = "Datos del registro no existe" });

                model = _mapper.Map<Incendio>(dto);

                var response = await _incendioServices.UpdateAsync(model);

                var resultSave = await _incendioServices.SaveAsync();

                if (!resultSave.Success)
                    return BadRequest(new OperationResult() { Success = false, Message = "Registro no se pudo guardar.", StatusCode = HttpStatusCode.BadRequest });


                var result = new OperationResult<Incendio>()
                {
                    Result = model,
                    Success = true,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Registro actualizado  con éxito."
                };
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
            }

        }

        // Update: api/Incendio/5
        /// <summary>
        /// Metodo utilizado para eliminar los Bomberos
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <response code="401">Unauthorized. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="201">OK. Devuelve el objeto creado.</response>        
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response> 
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.BadRequest, Message = "Campo Id es requerido" });

                //var model = await _incendioServices.GetByIdAsync(id);
                var model = _incendioServices.FindBy(e => e.Id == id).FirstOrDefault();

                var response = await _incendioServices.DeleteAsync(model);

                var resultSave = await _incendioServices.SaveAsync();

                if (!resultSave.Success)
                    return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = "Registro no se pudo eliminar" });


                var result = new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = true, Message = "Registro eliminado con éxito" };

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(new OperationResult() { StatusCode = HttpStatusCode.InternalServerError, Success = false, Message = $"Hubo  un error al cargar ,{e.Message}" });
            }

        }
    }
}
