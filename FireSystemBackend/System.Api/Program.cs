namespace System.Api
{
    public class Program
    {
        private static readonly string Namespace = typeof(Startup).Namespace;

        private static readonly string AppName = Namespace?[(Namespace.LastIndexOf('.',
            Namespace.LastIndexOf('.') - 1) + 1)..] ?? "";

        private static IConfiguration Configuration { get; set; }
        public static void Main(string[] args)
        {

            try
            {
                var host = CreateHostBuilder(args).Build();
                host.Run();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
               
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}