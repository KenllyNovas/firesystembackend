﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Infrastructure.Base
{
    public class PaginatorBase
    {
        public int? Codigo { get; set; }
        public int Page { get; set; }
        public int Take { get; set; }
        public string? OrderBy { get; set; }
    }
}
