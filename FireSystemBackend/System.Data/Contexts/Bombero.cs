﻿using System;
using System.Collections.Generic;

namespace System.DataModel.Contexts
{
    public partial class Bombero
    {
        public Bombero()
        {
            Incendios = new HashSet<Incendio>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; } = null!;
        public string Apellido { get; set; } = null!;
        public DateTime FechaNacimiento { get; set; }

        public virtual ICollection<Incendio> Incendios { get; set; }
    }
}
