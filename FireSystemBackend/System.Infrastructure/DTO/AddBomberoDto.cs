﻿using System;
using System.Collections.Generic;
using System.DataModel.Contexts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Infrastructure.DTO
{
    public class AddBomberoDto
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public string? Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }

        public  ICollection<AddIncendioDto>? Incendios { get; set; }
    }
}
