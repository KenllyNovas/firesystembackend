﻿namespace System.Infrastructure.DTO
{
    public class AddIncendioDto
    {
        public int Id { get; set; }
        public string Direccion { get; set; }
        public int BomberoId { get; set; }
        public DateTime Fecha { get; set; }


    }
}
