﻿using AutoMapper;
using System.DataModel.Contexts;
using System.Infrastructure.Base;
using System.Infrastructure.DTO;

namespace System.Infrastructure.Mapping
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Bombero, GetBomberoDto>().ReverseMap();
            CreateMap<Bombero, AddBomberoDto>().ReverseMap();

            CreateMap<Incendio, AddIncendioDto>().ReverseMap();
            CreateMap<Incendio, GetIncendioDto>().ReverseMap();
            CreateMap<Incendio, GetIncendioDto>()
             .ForMember(x => x.Nombre, o => o.MapFrom(p => p.Bombero.Nombre))
             .ForMember(x => x.Apellido, o => o.MapFrom(p => p.Bombero.Apellido))
             .ForMember(x => x.FechaNacimiento, o => o.MapFrom(m => m.Bombero.FechaNacimiento))
             .ReverseMap()
             .ForMember(x => x.Bombero, o => o.Ignore());

            CreateMap<DataCollection<Bombero>, DataCollectionDto<GetBomberoDto>>().ReverseMap();

            CreateMap<DataCollection<Incendio>, DataCollectionDto<GetIncendioDto>>().ReverseMap();
        }
    }
}
