﻿using Microsoft.EntityFrameworkCore;
using System.Data;
using System.DataModel.Contexts;
using System.Infrastructure.IRepository;
using System.Infrastructure.Mapping;
using System.Infrastructure.Repository;
using System.Infrastructure.Sevices;

namespace System.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
           services.AddAutoMapper(typeof(MappingProfiles));

            #region Configuration 
            services.AddCors(options =>
            {
                options.AddPolicy("Todos",
                builder => builder.WithOrigins("*").WithHeaders("*").WithMethods("*"));
            });
            services.ConfigureDbContext(Configuration);
            services.ServicesImplementations();
            services.ConfigureAutomapper();
            services.ConfigureSwagger(Configuration);
            services.ConfigureAddControllers(); 
           
            #endregion

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStaticFiles();

            }


            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SystemApi v1");
            });
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors("Todos");
            app.UseAuthorization();
            app.UseAuthentication();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
