using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Api.Controllers;
using System.Collections.Generic;
using System.DataModel.Contexts;
using System.Infrastructure.Base;
using System.Infrastructure.DTO;
using System.Infrastructure.IRepository;
using System.Infrastructure.Repository;
using System.Infrastructure.Sevices;
using System.Net;
using System.Threading.Tasks;

namespace WebApi.test.PruebasUnitarias
{
    [TestClass]
    public class BomberoControllerTests
    {
        

        [TestMethod]
        public async Task GetAllData()
        {
            var mockBomberoServices = new Mock<BomberoRepository>();
            var mockMapper = new Mock<IMapper>();

            var mockBomberos = new List<Bombero>
            {
                new Bombero { Id = 1, Nombre = "Bombero1", Incendios = new List<Incendio>() },
                new Bombero { Id = 2, Nombre = "Bombero2", Incendios = new List<Incendio>() }
            };

            mockBomberoServices.Setup(s => s.GetAllAsync()).ReturnsAsync(mockBomberos);

            var expectedResponse = new List<GetBomberoDto>
            {
                new GetBomberoDto { Id = 1, Nombre = "Bombero1" },
                new GetBomberoDto { Id = 2, Nombre = "Bombero2" }
            };

            mockMapper.Setup(m => m.Map<List<GetBomberoDto>>(mockBomberos)).Returns(expectedResponse);

            var controller = new BomberoController(mockBomberoServices.Object, mockMapper.Object);

            var result = await controller.GetAll();

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            var okResult = (OkObjectResult)result;
            var model = okResult.Value as List<GetBomberoDto>;

            Assert.IsNotNull(model); 
            Assert.AreEqual(expectedResponse.Count, model.Count); 
        }

        [TestMethod]
        public async Task DeleteReturnsOkResult()
        {
            var bomberos = new List<Bombero>
            {
                new Bombero { Id = 1  }
            };

            var fakeBomberoService = new FakeBomberoService(bomberos);
            var controller = new BomberoController(fakeBomberoService);

            var id = 1; 

            var result = await controller.Delete(id);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            var okResult = (OkObjectResult)result;
            var operationResult = (OperationResult)okResult.Value;
            Assert.AreEqual(HttpStatusCode.OK, operationResult.StatusCode);
            Assert.IsTrue(operationResult.Success);
            Assert.AreEqual("Registro eliminado con �xito", operationResult.Message);
        }


        [TestMethod]
        public async Task PutRequest()
        {
          
            var mockBomberoServices = new Mock<IBomberoRepository>();
            var mockMapper = new Mock<IMapper>();

            var controller = new BomberoController(mockBomberoServices.Object, mockMapper.Object);

            var dto = new AddBomberoDto { Id = 0  };
                       
            var result = await controller.Put(dto);
                        
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            var badRequestResult = (BadRequestObjectResult)result;
            var operationResult = (OperationResult)badRequestResult.Value;
            Assert.AreEqual(HttpStatusCode.BadRequest, operationResult.StatusCode);
            Assert.IsFalse(operationResult.Success);
            Assert.AreEqual("Campo Id es requerido", operationResult.Message);
        }

        [TestMethod]
        public async Task Post_WithNullDto_ReturnsBadRequest()
        {
            
            var mockBomberoServices = new Mock<IBomberoRepository>();
            var mockMapper = new Mock<IMapper>();

            var controller = new BomberoController(mockBomberoServices.Object, mockMapper.Object);

           
            var result = await controller.Post(null);

           
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            var badRequestResult = (BadRequestObjectResult)result;
            var operationResult = (OperationResult)badRequestResult.Value;
            Assert.AreEqual(HttpStatusCode.BadRequest, operationResult.StatusCode);
            Assert.IsFalse(operationResult.Success);
            Assert.AreEqual("Datos del modelo son requeridos.", operationResult.Message);
        }
    }
}