﻿using System;
using System.Collections.Generic;

namespace System.DataModel.Contexts
{
    public partial class Incendio
    {
        public int Id { get; set; }
        public string Direccion { get; set; } = null!;
        public int BomberoId { get; set; }
        public DateTime Fecha { get; set; }

        public virtual Bombero Bombero { get; set; } = null!;
    }
}
