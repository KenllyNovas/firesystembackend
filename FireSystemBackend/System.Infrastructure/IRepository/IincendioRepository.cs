﻿
using System.DataModel.Contexts;

namespace System.Infrastructure.IRepository
{
    public interface IincendioRepository : IGenericRepository<Incendio>
    {
        IQueryable<Incendio> GetIncendios(long Id);
    }
}
