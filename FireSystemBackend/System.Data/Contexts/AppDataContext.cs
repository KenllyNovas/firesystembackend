﻿using Microsoft.EntityFrameworkCore;

namespace System.DataModel.Contexts
{
    public partial class AppDataContext : DbContext
    {
        public AppDataContext()
        {
        }

        public AppDataContext(DbContextOptions<AppDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bombero> Bomberos { get; set; } = null!;
        public virtual DbSet<Incendio> Incendios { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=LAPGABAGUA-01\\SQLEXPRESS;Initial Catalog=dbBomberos;Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bombero>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaNacimiento).HasColumnType("date");
            });

            modelBuilder.Entity<Incendio>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Fecha).HasColumnType("date");

                entity.HasOne(d => d.Bombero)
                    .WithMany(p => p.Incendios)
                    .HasForeignKey(d => d.BomberoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Incendios__Bombe__267ABA7A");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
